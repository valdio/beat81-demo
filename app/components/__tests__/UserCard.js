import React from 'react';
import renderer from 'react-test-renderer';
import UserCard from '../UserCard';
import {shallow} from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {userMock} from './mock';
import {Text, TouchableOpacity} from 'react-native';

Enzyme.configure({adapter: new Adapter()});
const wrapper = shallow(<UserCard data={userMock}/>);

it('renders UserCard using Snapshot', () => {
  const tree = renderer.create(<UserCard data={userMock}/>);
  expect(tree).toMatchSnapshot();
});

describe('UserCard is a Button/TouchableOpacity', () => {
  expect(wrapper.find(TouchableOpacity).length).toBe(1);
});

describe('UserCard renders user`s fullname and status', () => {
  expect(wrapper.find(Text).length).toBe(2);
});
