import React from 'react';
import renderer from 'react-test-renderer';
import {workout} from '../workout';
import * as types from '../../actions/types';
import {userMock, workoutMock} from './mock';

test('Test workouts reducer list ', () => {
  //general case
  expect(workout({}, {type: types.WORKOUT_LIST, workouts: [workoutMock]})).toEqual({
    list: [workoutMock],
  });

  //special cases
  expect(workout({}, {type: types.WORKOUT_LIST, workouts: []})).toEqual({
    list: [],
  });

  expect(workout({}, {type: types.WORKOUT_LIST, workouts: undefined})).toEqual({
    list: [],
  });
});

test('Test user status update on workout', () => {
  //general case
  expect(workout({list: [workoutMock]}, {
    type: types.WORKOUT_UPDATE,
    userID: userMock.id,
    workoutID: workoutMock.id,
    status: 'canceled',
  })).toEqual({
    list: [{
      ...workoutMock,
      attendance: [
        ...(workoutMock.attendance || []).filter(item => item.id !== userMock.id),//all but the user we are updating
        {...workoutMock.attendance.find(item => item.id === userMock.id), status: 'canceled'},
      ],
    },
    ],
  });

  //special cases
  expect(workout({list: [workoutMock]}, {
    type: types.WORKOUT_UPDATE,
    status: 'canceled',
  })).toEqual({
    list: [workoutMock],
  });

  expect(workout({list: [workoutMock]}, {
    type: types.WORKOUT_UPDATE,
    userID: 'invalid ID',
    workoutID: workoutMock.id,
    status: 'canceled',
  })).toEqual({
    list: [workoutMock],
  });

  expect(workout({list: [workoutMock]}, {
    type: types.WORKOUT_UPDATE,
    userID: userMock.id,
    workoutID: 'invalid ID',
    status: 'canceled',
  })).toEqual({
    list: [workoutMock],
  });
});
