import React from 'react';
import renderer from 'react-test-renderer';
import WorkoutCard from '../WorkoutCard';
import {shallow} from 'enzyme';
import Enzyme from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {workoutMock} from './mock';
import FastImage from 'react-native-fast-image';
import {Text} from 'react-native';

Enzyme.configure({adapter: new Adapter()});
const wrapper = shallow(<WorkoutCard data={workoutMock}/>);

it('renders WorkoutCard using Snapshot', () => {
  const tree = renderer.create(<WorkoutCard data={workoutMock}/>);
  expect(tree).toMatchSnapshot();
});

describe('WorkoutCard has cover image', () => {
  expect(wrapper.find(FastImage).length).toBe(1);
});

describe('WorkoutCard has workout title', () => {
  expect(wrapper.find(Text).length).toBe(1);
});
