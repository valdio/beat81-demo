export const users = [
  {
    id: 1,
    fullname: 'Valdio Veliu',
    email: 'test@test.com',
  },
  {
    id: 2,
    fullname: 'Test user',
    email: 'testsjsj@test.com',
  },
  {
    id: 3,
    fullname: 'Hello user',
    email: 'test@test.com',
  },
  {
    id: 4,
    fullname: 'Lorem ipsum',
    email: 'test@st.com',
  },
  {
    id: 5,
    fullname: 'Some random user',
    email: 'test@test.com',
  },
];

export const workouts = [
  {
    id: 1,
    name: 'Workout 1',
    cover: 'http://lorempixel.com/640/480/',
    attendance: [...users.map(user => ({...user, status: 'checked-in'}))], //check in all users in all workouts
  },
  {
    id: 2,
    name: 'Workout 2',
    cover: 'http://lorempixel.com/640/480/',
    attendance: [...users.map(user => ({...user, status: 'checked-in'}))],
  },
  {
    id: 3,
    name: 'Workout 3',
    cover: 'http://lorempixel.com/640/480/',
    attendance: [...users.map(user => ({...user, status: 'checked-in'}))],
  },
];
