//action type of redux persist. Used to handle state updates on re-hydration.
export const REHYDRATE = 'persist/REHYDRATE';

export const WORKOUT_LIST = 'WORKOUT_LIST';
export const WORKOUT_UPDATE = 'WORKOUT_UPDATE';
