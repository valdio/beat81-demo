import {ROUTE} from './routes';
import Home from '../../containers/Home';
import Workout from '../../containers/Workout';

//App main routing screens
export const globalRoutes = [
  {
    route: ROUTE.INDEX,
    component: Home,
  },
  {
    route: ROUTE.WORKOUT,
    component: Workout,
  },
];
