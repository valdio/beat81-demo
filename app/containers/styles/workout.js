import React from 'react';
import {COLORS, Spacing, Typography} from '../../template';
import {DEVICE} from '../../lib/device';

const coverHeight = DEVICE.HEIGHT * 0.3;
export default {
  container: {
    flex: 1,
  },
  content: {
    flex: 1,
  },
  cover: {
    backgroundColor: COLORS.GREEN,
    height: coverHeight,
    width: DEVICE.WIDTH,
  },
  userList: {
    ...Spacing.horizontalSpacing,
  },
  title: {
    ...Typography.pageTitle,
    ...Spacing.horizontalSpacing,
    ...Spacing.verticalSpacing,
    alignSelf: 'flex-end',
  },
};
