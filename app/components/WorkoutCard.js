import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {COLORS} from '../template';
import FastImage from 'react-native-fast-image';
import {DEVICE} from '../lib/device';
import {Spacing, Typography} from '../template';
import {Link} from 'react-router-native';
import {ROUTE} from '../lib/routing/routes';

export default WorkoutCard = ({data}) =>
  <Link style={{flex: 1}} underlayColor={COLORS.BACKGROUND} to={`${ROUTE.WORKOUT_TAG}/${data.id}`}>
    <View style={styles.container}>
      <FastImage
        style={styles.cover}
        source={{uri: data.cover || '', priority: FastImage.priority.normal}}
        resizeMode={FastImage.resizeMode.cover}
      />

      <View style={styles.content}>
        <Text style={styles.title}>{data.name}</Text>
      </View>
    </View>
  </Link>;

export const styles = StyleSheet.create({
  container: {
    ...Spacing.horizontalSpacing,
    ...Spacing.verticalSpacing,
    height: DEVICE.HEIGHT * 0.45,
    flex: 1,
    overflow: 'hidden',
    alignItems: 'flex-end',
    backgroundColor: COLORS.BLUE,
    borderRadius: 5,
    flexDirection: 'row',
  },
  content: {
    flex: 1,
    backgroundColor: COLORS.BLUE,
    opacity: 0.8,
    padding: 6,
    alignItems: 'flex-end',
  },
  cover: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
  },
  title: {
    ...Typography.sectionTitle,
    color: COLORS.BLACK,
    alignSelf: 'flex-end',
  },
});
