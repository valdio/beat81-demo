# Beat81 Demo Mobile app

***Technology:*** React Native.

#### Used libraries:
* redux
* react-redux
* redux-logger
* redux-thunk
* redux-persist
* react-router-native
* react-native-fast-image - Image caching

# Testing

Testing configured with:
- jest
- enzyme

```
#to run tests
cd projectFolder/
npm test
```

# Run project:

```
cd projectFolder/
npm install 
#OR 
yarn

# start react-native server
react-native start
react-native start --reset-cache

# Run app
react-native run-android
react-native run-ios

# Logging system
react-native log-android
react-native log-ios
```


Logging enabled in development only.
Open console at:
[http://localhost:8081/debugger-ui/](http://localhost:8081/debugger-ui/)

# Building For production

## Android

For Android fake production keys and keychain are included in the project and the app is ready for production build at any time.

Instructions:

```

cd projectFolder/
npm install
cd android

#clean app

./gradlew clean

#build for production

./gradlew assembleRelease 

```


The build APK is located at `/android/app/build/outputs/apk/`

## iOS

iOS build can be done using xCode.

