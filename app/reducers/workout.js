import createReducer from '../lib/createReducer';
import * as types from '../actions/types';

export const workout = createReducer({}, {
  // Can be used to catch the redux re-hydration event and reset the redux cache if needed. Used in development only!
  // [types.REHYDRATE](state, action) {
  //   return {
  //     ...state
  //   }
  // },
  [types.WORKOUT_LIST](state, action) {
    return {...state, list: action.workouts || []};
  },
  [types.WORKOUT_UPDATE](state, action) {
    if (!action.workoutID || !action.userID || !action.status) {
      return state;
    }
    const userWorkout = state.list.find(item => item.id === action.workoutID) || {};
    if (!userWorkout.id) {
      return state;//no valid workout found
    }

    const updatedWorkoutAttendance = (userWorkout.attendance || []).map(item =>
      (item.id === action.userID ? {...item, status: action.status} : item));
    return {
      ...state,
      list: [...(state.list || []).filter(item => item.id !== action.workoutID), {
        ...userWorkout, attendance: updatedWorkoutAttendance,
      }],
    };
  },

});
