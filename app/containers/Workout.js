import React, {Component} from 'react';
import {View, Text, ScrollView, FlatList} from 'react-native';
import {connect} from 'react-redux';
import styles from './styles/workout';
import FastImage from 'react-native-fast-image';
import UserCard from '../components/UserCard';

class Workout extends Component {
  constructor(props) {
    super(props);
    const workoutID = Number(this.props.match.params.id) || 1;
    this.state = {
      workoutID,
    };
  }

  render() {
    const workout = this.props.workouts.find(item => item.id === this.state.workoutID) || {};

    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View>
            <FastImage
              style={styles.cover}
              source={{uri: workout.cover || '', priority: FastImage.priority.normal}}
              resizeMode={FastImage.resizeMode.cover}
            />
            <View style={styles.content}>
              <Text style={styles.title}>{workout.name}</Text>
              <FlatList
                style={styles.userList}
                data={workout.attendance}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item, index}) => (
                  <UserCard
                    data={item}
                    onPress={() => {
                      this.props.setUserStatusForWorkout(item.id, workout.id, (item.status === 'checked-in' ? 'canceled' : 'checked-in'));
                      this.props.emmitSocketMessage('update_attendance', {
                        userID: item.id,
                        workoutID: workout.id,
                        status: (item.status === 'checked-in' ? 'canceled' : 'checked-in'),
                      });
                    }}
                  />
                )}
              />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state) {
  return {
    workouts: (state.workout && state.workout.list) || [],
  };
}

export default connect(mapStateToProps)(Workout);
