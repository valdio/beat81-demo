import {users} from '../../assets/mock';

export const userMock = {id: 1, fullname: 'Test user', status: 'checked-in'};

export const workoutMock = {
  id: 1,
  name: 'TEST workout',
  cover: 'http://lorempixel.com/640/480/',
  attendance: [userMock],
};
