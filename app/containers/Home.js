import React, {Component} from 'react';
import {View, FlatList} from 'react-native';
import {connect} from 'react-redux';
import styles from './styles/home';
import WorkoutCard from '../components/WorkoutCard';

class Home extends Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.getWorkoutsList();
  }

  render() {
    return (<View style={styles.container}>
      <FlatList
        numColumns={2}
        data={this.props.workouts}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item, index}) => <WorkoutCard data={item}/>}
      />
    </View>);
  }
}

function mapStateToProps(state) {
  return {
    workouts: (state.workout && state.workout.list && state.workout.list.sort((a, b) => a.id - b.id)) || [],
  };
}

export default connect(mapStateToProps)(Home);
