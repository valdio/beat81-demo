export const ROUTE = {
  //Global navigation routes
  INDEX: '/',
  WORKOUT: '/workout/:id',
  WORKOUT_TAG: '/workout',
};
