import * as types from './types';
import Api from '../lib/api';
import {buildResponse} from '../lib/responseObject';
import {workouts} from '../assets/mock';

/**
 * Get a list of all workouts.
 * @param callback
 * @returns {function(*, *): Promise<T>}
 */
export function getWorkoutsList(callback) {
  return (dispatch, getState) => {
    //NORMAL USE-CASE example WHEN WORKING WITH API
    // return Api.get(`/workouts`).then(response => {
    //   if (response && response.data) {
    //     callback && callback(buildResponse(response, undefined));
    //     dispatch(workoutsReceived(response, start === 1));
    //   } else {
    //     callback && callback(buildResponse(undefined, response.error || 'Default error message'));
    //   }
    // }).catch((exception) => {
    //   //handle exception
    //   callback && callback(buildResponse(undefined, 'Default error message'));
    // });

    //DEMO USE CASE
    const workoutsCache = getState().workout && getState().workout.list || [];
    if (!workoutsCache || workoutsCache.length === 0) {
      dispatch(workoutsReceived(workouts));
    }
  };
}

function workoutsReceived(workouts) {
  return {
    type: types.WORKOUT_LIST,
    workouts,
  };
}

/**
 * Set the user status for a workout.
 * @param userID
 * @param workoutID
 * @param status checked-in || canceled
 * @param callback
 * @return {Function}
 */
export function setUserStatusForWorkout(userID, workoutID, status, callback) {
  return (dispatch, getState) => {
    //NORMAL USE-CASE example WHEN WORKING WITH API - SOMETHING LIKE THE FOLLOWING.

    // return Api.post(`/workout/${workoutID}/`, {userID, status}).then(response => {
    //   if (response && response.data) {
    //     callback && callback(buildResponse(response, undefined));
    //     dispatch(updateWorkout(userID, workoutID, status));
    //   } else {
    //     callback && callback(buildResponse(undefined, response.error || 'Default error message'));
    //   }
    // }).catch((exception) => {
    //   //handle exception
    //   callback && callback(buildResponse(undefined, 'Default error message'));
    // });

    //DEMO USE CASE
    dispatch(updateWorkout(userID, workoutID, status));
  };
}

function updateWorkout(userID, workoutID, status) {
  return {
    type: types.WORKOUT_UPDATE,
    userID,
    workoutID,
    status,
  };
}
