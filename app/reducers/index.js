import {combineReducers} from 'redux';
import * as workoutReducer from './workout';

export default combineReducers(Object.assign(
  workoutReducer,
));
