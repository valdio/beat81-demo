import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {COLORS} from '../template';
import {Spacing, Typography} from '../template';

export default UserCard = ({data, onPress}) =>
  <TouchableOpacity style={styles.container} onPress={onPress}>
    <View style={styles.content}>
      <Text style={styles.fullnameText}>{data.fullname}</Text>
      <Text
        style={[styles.statusText, {...(data.status === 'checked-in' ? {color: COLORS.GREEN} : {color: COLORS.RED})}]}>{data.status}</Text>
    </View>
  </TouchableOpacity>;

export const styles = StyleSheet.create({
  container: {
    elevation: 3,
    marginBottom: 4,
  },
  content: {
    ...Spacing.verticalSpacing,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  fullnameText: {
    ...Typography.sectionTitle,
    alignSelf: 'flex-start',
  },
  statusText: {
    ...Typography.text,
  },
});
