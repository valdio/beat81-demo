export const base = {
  marginVertical: 20,
};

export const horizontalSpacing = {
  marginHorizontal: 12,
};

export const verticalSpacing = {
  marginVertical: 6,
};

export const section = {
  ...horizontalSpacing,
  marginVertical: 10,
};


